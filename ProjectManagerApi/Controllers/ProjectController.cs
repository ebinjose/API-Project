﻿#region Assemblies
using BusinessEntities;
using BusinessLayer;
using System.Collections.Generic;
using System.Web.Http;
#endregion

namespace ProjectManagerApi.Controllers
{
    public class ProjectController : ApiController
    {
        /// <summary>
        /// Instance of business layer class 
        /// </summary>
        private readonly ProjectBusiness oProject;

        /// <summary>
        /// Constructor of the Controller
        /// </summary>
        public ProjectController()
        {
            oProject = new ProjectBusiness();
        }

        /// <summary>
        /// Service call to insert or update projects
        /// </summary>
        /// <param name="oProj"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/updateProject")]
        public ProjectUpdateResult Post(ProjectModel oProj)
        {
            return oProject.UpdateProject(oProj);
        }

        /// <summary>
        /// Service call to get all projects
        /// </summary>        
        [HttpGet]
        [Route("api/getAllProjects")]
        public IEnumerable<ProjectModel> Get()
        {
            return oProject.GetAllProject();
        }

    }
}