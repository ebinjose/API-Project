﻿#region Assemblies
using BusinessEntities;
using BusinessLayer;
using System.Collections.Generic;
using System.Web.Http;
#endregion

namespace ProjectManagerApi.Controllers
{
    public class UserController : ApiController
    {
        /// <summary>
        /// Instance of User business layer class 
        /// </summary>
        private readonly UserBusiness userBusiness;

        public UserController()
        {
            userBusiness = new UserBusiness();
        }

        /// <summary>
        /// Service call to get all users
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/getAllUsers")]
        public IEnumerable<UserModel> Get()
        {
          return userBusiness.GetAllUsers();
        }


        /// <summary>
        /// Service call to deletel user
        /// </summary>
        /// <param name="oUser"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/deleteUser")]
        public Status DeleteUser(UserModel oUser)
        {
            return userBusiness.DeleteUser(oUser);
        }

        /// <summary>
        /// Service call to update user
        /// </summary>
        /// <param name="oUser"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/updateUser")]
        public UserUpdateResult UpdateUser(UserModel oUser)
        {
            return userBusiness.UpdateUser(oUser);

        }
    }
}